<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:2|unique:users',
            'password' => 'required|min:5',
            'fullname' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user = new User();
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->fullname = $request->fullname;
        $user->save();
        $token = Auth::login($user);

        return response()->json(['token' => $token], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['username', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['errors' => ['login' => ['Invalid username or password']]], 401);
        }

        return response()->json(['token' => $token], 200);
    }

    public function getUserList(Request $request)
    {
        $users = User::paginate(10);

        return response()->json($users, 200);
    }
}
